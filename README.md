# Estilo de apresentação para a UNIPAMPA

Este estilo serve para criar apresentações com o LaTeX Beamer.

Compile a apresentação com o xelatex ao invés do pdflatex ou latex. E instale a fonte [Fontin Sans](http://www.exljbris.com/fontinsans.html).

